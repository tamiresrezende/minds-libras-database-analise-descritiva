A base de dados foi criada após um processo de escolha criteriosa dos parâmetros que a compõem (sinais, sinalizadores, sensores, ambiente de gravação), juntamente com a definição de um protocolo de gravação. Realizar a análise descritiva da Libras-20 finaliza e completa o processo de criação da base, pois permite descrever com detalhes seus aspectos e a entender o comportamento dos dados. Este estudo apresenta as características relevantes dos dados visando a sua aplicação em sistemas de reconhecimento automático de sinais de Libras e reconhecimento de gestos. 

Para maiores informações, acesse: [Minds Lab - BRAZILIAN SIGN LANGUAGE RECOGNITION](https://minds.eng.ufmg.br/project/4)

Databases: [MINDS-Libras Database (câmera RGB)](https://doi.org/10.5281/zenodo.2667329) e [MINDS-Libras Dataset (RGB-D sensor data)](https://doi.org/10.5281/zenodo.4322984
)
